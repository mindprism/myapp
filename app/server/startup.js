import { Meteor } from 'meteor/meteor';
//const pt= require('periodic-table');
const pt = Meteor.npmRequire('periodic-table');

let ToeSingleFeatures = {};
let ToeSingleFeatures_a;

// helper - server only
const getFeatures = function(subject, single_features, array_features, string_features) {
  const subject_keys = Object.keys(subject);
  const o = {
    'number': single_features
    , 'object': array_features
    , 'string': string_features
  };
  for (let j = 0; j < subject_keys.length; j++) {
    //console.log("type of " + subject_keys[j] + " is " + typeof(subject[subject_keys[j]]));
    // only use features that are numbers ... ignore arrays etc.
    let itm = subject[subject_keys[j]];
    let type = typeof(itm);
    if (typeof o[type] !== 'undefined') {
      const doit = (type === 'object' && itm.length !== undefined) || type !== 'object';
      if (doit) {
        o[type][subject_keys[j]] = itm;
      }
    }
  }
};

// server only
const initToesSingles = function() {
  const elems = pt.elements;
  for (let name in elems) {
    if (!elems.hasOwnProperty(name)) {
      continue;
    }
    const el = Object.assign({}, elems[name]);
    const single_features = {};
    const array_features = {};
    const string_features = {};
    getFeatures(el, single_features, array_features, string_features);
    ToeSingleFeatures = Object.assign({}, ToeSingleFeatures, single_features);
  }
  delete ToeSingleFeatures.cpkHexColor;
  for (let key in ToeSingleFeatures) {
    ToeSingleFeatures[key] = 0;
  }
};

// server only
const initToes = function() {
  //Toes.remove({});
  initToesSingles();
  let inserted_toes = 0;
  if (!Toes.findOne()) {
    const elems = pt.elements;
    for (let name in elems) {
      if (!elems.hasOwnProperty(name)) {
        continue;
      }
      const el = Object.assign({}, elems[name]);
      const single_features = {};
      const array_features = {};
      const string_features = {};
      getFeatures(el, single_features, array_features, string_features);
      el.single_features = Object.assign({}, ToeSingleFeatures, single_features);
      Toes.insert(el);
      inserted_toes++;
    }
    console.log("Inserted " + inserted_toes + " new elements...");
  }
};

// define a startup script that initializes Toes db
if (Meteor.isServer) {
  //==================================================
  Meteor.startup(function() {
    console.log("Meteor.startup...");
    //initSongs();
    initToes();
  });
  //==================================================
  Meteor.methods({
    webstorm:function(){
      this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.ToeSingleFeatures); // !unuf - lint hack for webstorm
    },
    ToeSingleFeatures:function(){
      if (ToeSingleFeatures_a === undefined) {//cache
        const a = [];
        for (let name in ToeSingleFeatures) {
          a.push({name:name});
        }
        ToeSingleFeatures_a=a;
      }
      return ToeSingleFeatures_a;
    }
  });//-Meteor.methods
}//-Meteor.isServer
