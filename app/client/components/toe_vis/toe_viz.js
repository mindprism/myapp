/* global vis */
const vis=require("vis");
const tinycolor = require("tinycolor2");

// this variable will store the visualisation so we can delete it when we need to
let visjsobj;
//==================================================
// client only
if (Meteor.isClient){
  // setup pick list values
  Meteor.call('ToeSingleFeatures', function(err, data) {
    if (err){
      console.log(err);
    }
    Session.set('ToeSingleFeatures', data);
  });//-Meteor.call

  //==================================================
  // Helpers for controls
  Template.toe_viz_controls.helpers({
    webstorm: function(){
      this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.get_feature_names); // !unuf - lint hack for webstorm
      this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.webstorm); // !unuf - lint hack for webstorm
    },
    // returns an array of the names of all features of the requested type
    get_feature_names : function(){
      return Session.get("ToeSingleFeatures");
    }
  });//-Template.toe_viz_controls.helpers

  //==================================================
  // Helpers for feature list
  Template.toe_feature_list.helpers({
    "feature":function(){
      if (Session.get("feature") != undefined) {
        return Session.get("feature").name;
      }
      return '';
    },
    "get_all_feature_values":function(){
      if (Session.get("feature") != undefined){
        const toes = Toes.find({});
        const features = [];
        let ind = 0;
        const feature=Session.get("feature");
        // build an array of data on the fly for the
        // template consisting of 'feature' objects
        // describing the song and the value it has for this particular feature
        //featureFormat(value,feature)
        toes.forEach(function(toe){
          //console.log(song);
          const value=toe[feature.type][feature.name];
          features[ind] = {
            name:toe.name,
            symbol:toe.symbol,
            value:featureFormat(value,feature),
            foreColor:colorFor(toe,'font'),
            backColor:colorFor(toe,'font')
          };
          ind ++;
        });
        function sorter(a,b){
          a=parseFloat(''+a.value);
          b=parseFloat(''+b.value);
          if (a>b) {
            return 1;
          }
          if (a<b) {
            return -1;
          }
          return 0;
        }
        features.sort(sorter);
        return features;
      }
      else {
        return [];
      }
    }
  });//-Template.toe_feature_list.helpers

  //==================================================
  // event handlers for the viz control form
  Template.toe_viz_controls.events({
    //==================================================
    // user changes the selected option in the drop down list
    "change .js-select-single-feature":function(event){
      event.preventDefault();
      const feature = $(event.target).val();
      Session.set("feature", {name:feature, type:"single_features"});
    },//-"change .js-select-single-feature"

    //==================================================
    // user clicks on the blobs button
    "click .js-show-blobs":function(event){
      event.preventDefault();
      initBlobVis();
    },//-"click .js-show-blobs"

    //==================================================
    // user clicks on the timeline button
    "click .js-show-timeline":function(event){
      event.preventDefault();
      initDateVis();
    },//-"click .js-show-timeline"

    //==================================================
    // user clicks on the graph button
    "click .js-show-graph":function(event){
      event.preventDefault();
      initGraphVis();
    },//-"click .js-show-timeline"
  });//-Template.toe_viz_controls.events

}//-Meteor.isClient


// const deleteNamedStyle=function(name_) {
//   updateNamedStyle.namedStyles=updateNamedStyle.namedStyles?updateNamedStyle.namedStyles:[];
//   var i=updateNamedStyle.namedStyles.indexOf(name_);
//   if (i!==-1) {
//     updateNamedStyle.namedStyles=updateNamedStyle.namedStyles.splice(i,1);
//     var qo=$('#'+name_);
//     if (qo.length==1) {
//       qo.remove();
//     }else{
//       if (qo.length>1) {
//         console.error('multiple element matches for named style:'+name_);
//         qo.remove();
//       }else{
//         console.error('zero element matches for named style:'+name_);
//       }
//     }
//   }else{
//     console.warn('named style not found:'+name_);
//   }
// } //-deleteNamedStyle
const updateNamedStyle=function(style_str_,name_) {
  if (name_===undefined) {
    name_='defaultStyle';
  }
  updateNamedStyle.namedStyles=updateNamedStyle.namedStyles?updateNamedStyle.namedStyles:[];
  if (updateNamedStyle.namedStyles.indexOf(name_)===-1) {
    updateNamedStyle.namedStyles.push(name_);
  }
  const st = document.getElementById(name_);
  if (st!==null) {
    //st=st[0];
    st.innerHTML=style_str_;
  }else{
    const head = document.getElementsByTagName("HEAD")[0];
    const ele = head.appendChild(window.document.createElement('style'));
    ele.id=name_;
    ele.innerHTML = style_str_;
  }
}; //-updateNamedStyle


// function that creates a new timeline visualisation
function initDateVis(){
  // clear out the old visualisation if needed
  if (visjsobj != undefined){
    visjsobj.destroy();
  }
  const toes = Toes.find({});
  let ind = 0;
  // generate an array of items
  // from the songs collection
  // where each item describes a song plus the currently selected
  // feature
  const items = [];
  // iterate the songs collection, converting each song into a simple
  // object that the visualiser understands
  /*
    toe
    { atomicNumber: 1,
      symbol: 'H',
      name: 'Hydrogen',
      atomicMass: '1.00794(4)',
      cpkHexColor: 'FFFFFF',
      electronicConfiguration: '1s1',
      electronegativity: 2.2,
      atomicRadius: 37,
      ionRadius: '',
      vanDelWaalsRadius: 120,
      ionizationEnergy: 1312,
      electronAffinity: -73,
      oxidationStates: '-1, 1',
      standardState: 'gas',
      bondingType: 'diatomic',
      meltingPoint: 14,
      boilingPoint: 20,
      density: 0.0899,
      groupBlock: 'nonmetal',
      yearDiscovered: 1766
    }

  */
  let s='';
  toes.forEach(function(toe){
    //noinspection JSUnresolvedVariable
    if (toe.yearDiscovered != undefined){
      let label = "ind: " + ind;
      if (toe.name != undefined){// we have a name
        label = toe.name + " : " +
        toe.symbol;
      }
      const value = toe[Session.get("feature")["type"]][Session.get("feature")["name"]];
      console.log('toe.yearDiscovered',typeof toe.yearDiscovered,toe.yearDiscovered);
      //noinspection JSUnresolvedVariable
      if (toe.yearDiscovered!=="Ancient"&&toe.yearDiscovered!==""&&toe.yearDiscovered!==0) {
        //console.log('toe.yearDiscovered DID',typeof toe.yearDiscovered,toe.yearDiscovered);
        //console.log('value',value);
        //const date = '' + toe.yearDiscovered + "";
        // here we create the actual object for the visualiser
        // and put it into the items array
        items[ind] = {
          x: toe.yearDiscovered,
          y: value,
          // slightly hacky label -- check out the vis-label
          // class in toe_data_viz.css
          label:{content:label, className:'vis-label toe-label-'+toe.symbol, yOffset:-10,xOffset:10},
          color:{
            background:colorFor(toe,'background'),
            border:colorFor(toe,'border'),
            highlight:{
              background:colorFor(toe,'highlight.background'),
              border:colorFor(toe,'highlight.border'),
            },
            hover:{
              background:colorFor(toe,'hover.background'),
              border:colorFor(toe,'hover.border'),
            }
          },
          className:'toe-tick-'+toe.symbol
        };
        s+='.toe-label-'+toe.symbol+'{'+"\n";
        s+=' fill: '+colorFor(toe,'font')+'!important;'+"\n";
        s+='}'+"\n";
        s+='.toe-tick-'+toe.symbol+'{'+"\n";
        s+=' fill: '+colorFor(toe,'font')+'!important;'+"\n";
        s+='}'+"\n";
        ind ++ ;
      }
      updateNamedStyle(s,'toelabel');
    }
  });
  // set up the data plotter
  const options = {
    //style: 'bar',
    //barChart: {width:50, align:'center'}
    drawPoints:{
      style:'square',
      onRender: function(item, group, graph2d) {
        this["UNUSED FUNCTION\n"] && this["UNUSED FUNCTION\n"](this.onRender); // !unuf - lint hack for webstorm
        if (item.label == null) {
          return false;
        }
        return {
          style: 'square',
          size:10,
          className:item.className
        };
      }
    },
    shaded:{
      orientation:'bottom'
    }
  };
  // get the div from the DOM that we are going to
  // put our graph into
  const container = document.getElementById('visjs');
  // create the graph
  //noinspection JSUnresolvedFunction
  console.log(items);
  visjsobj = new vis.Graph2d(container, items, options);
  // tell the graph to set up its axes so all data points are shown
  visjsobj.fit();
}

function featureFormat(v,feature){
  function fix0(v){
    return parseFloat(''+v).toFixed(0);
  }
  function fix3(v){
    return parseFloat(''+v).toFixed(3);
  }
  function fix6(v){
    return parseFloat(''+v).toFixed(6);
  }
  const fmap={
    atomicNumber:fix0
    ,atomicRadius:fix0
    ,density:fix6
    ,electronAffinity:fix0
    ,electronegativity:fix3
    ,meltingPoint:fix0
    ,boilingPoint:fix0
    ,oxidationStates: fix0
    ,vanDelWaalsRadius:fix0
    ,ionizationEnergy:fix0
    ,yearDiscovered:fix0
    ,cpkHexColor:fix0
  };
  if (typeof fmap[feature.name]===undefined) {
    throw new Error('fmap[feature.name]===undefined for '+feature.name);
  }
  return fmap[feature.name](v);
}

// function that creates a new blobby visualisation
function initBlobVis(){
  // clear out the old visualisation if needed
  if (visjsobj != undefined){
    visjsobj.destroy();
  }
  // find all songs from the Songs collection
  const toes = Toes.find({});
  const nodes = [];
  let ind = 0;
  const feature=Session.get("feature");
  // iterate the songs, converting each song into
  // a node object that the visualiser can understand
  toes.forEach(function(toe){
    const value = toe[feature.type][feature.name];
    // set up a label with the song title and artist
    let label = "ind: " + ind;
    if (toe.name != undefined){// we have a name
      label = toe.symbol + " " + featureFormat(value,feature) + " " +
      toe.name;
    }
    // figure out the value of this feature for this song
    // create the node and store it to the nodes array
    nodes[ind] = {
      id:ind,
      label:label,
      value:value,
      font:{color:colorFor(toe,'font')},
      color:{
        background:colorFor(toe,'background'),
        border:colorFor(toe,'border'),
        highlight:{
          background:colorFor(toe,'highlight.background'),
          border:colorFor(toe,'highlight.border'),
        },
        hover:{
          background:colorFor(toe,'hover.background'),
          border:colorFor(toe,'hover.border'),
        }
      }
    };

    ind ++;
  });
  // edges are used to connect nodes together.
  // we don't need these for now...
  const edges = [];
  // this data will be used to create the visualisation
  const data = {
    nodes: nodes,
    edges: edges
  };
  // options for the visualisation
  const options = {
    nodes: {
      shape: 'dot',
    },
    layout: {
      improvedLayout:false
    }
  };
  // get the div from the dom that we'll put the visualisation into
  const container = document.getElementById('visjs');
  // create the visualisation
  //noinspection JSUnresolvedFunction
  visjsobj = new vis.Network(container, data, options);
}

// function that creates a new graph visualisation
function initGraphVis(){
  // clear out the old visualisation if needed
  if (visjsobj != undefined){
    visjsobj.destroy();
  }
  // find all songs from the Songs collection
  const toes = Toes.find({});
  const nodes = [];
  const edges = [];
  let ind = 0;
  const feature=Session.get("feature");
  let node={
    id:ind,
    label:"Elements",
    value:0,
    font:{
      color:"#aaaaaa"
    },
    color:"green"
  };
  nodes.push(node);
  ind++;
  //
  const blocks={};
  toes.forEach(function(toe){
    blocks[toe.groupBlock]=true;
  });
  const blockKeys=Object.keys(blocks);
  blockKeys.forEach(function(bk){
    let node={
      id:ind,
      label:bk,
      value:0,
      font:{
        color:"#aaaaaa"
      },
      color:"#444444"
    };
    nodes.push(node);
    edges.push({from:0,to:ind});
    blocks[bk]=ind;
    ind++;
  });

  // iterate the songs, converting each song into
  // a node object that the visualiser can understand
  toes.forEach(function(toe){
    const value = toe[feature.type][feature.name];
    // set up a label with the song title and artist
    let label = "ind: " + ind;
    if (toe.name != undefined){// we have a name
      label = toe.symbol + " " + featureFormat(value,feature) + " " +
        toe.name;
    }
    // figure out the value of this feature for this song
    // create the node and store it to the nodes array
    nodes[ind] = {
      id:ind,
      label:label,
      value:value,
      font:{color:colorFor(toe,'font')},
      color:{
        background:colorFor(toe,'background'),
        border:colorFor(toe,'border'),
        highlight:{
          background:colorFor(toe,'highlight.background'),
          border:colorFor(toe,'highlight.border'),
        },
        hover:{
          background:colorFor(toe,'hover.background'),
          border:colorFor(toe,'hover.border'),
        }
      }
    };
    edges.push({from:blocks[toe.groupBlock],to:ind});
    ind ++;
  });
  // this data will be used to create the visualisation
  const data = {
    nodes: nodes,
    edges: edges
  };
  // options for the visualisation
  const options = {
    nodes: {
      shape: 'dot',
    },
    layout: {
      improvedLayout:false
    }
  };
  // get the div from the dom that we'll put the visualisation into
  const container = document.getElementById('visjs');
  // create the visualisation
  //noinspection JSUnresolvedFunction
  visjsobj = new vis.Network(container, data, options);
}







//noinspection JSUnresolvedVariable
colorFor=function(toe,who){
  let c = '#aaaaaa';
  if (toe.cpkHexColor) {
    c='#'+toe.cpkHexColor;
  }
  const tc = tinycolor(c);
  if (who==='font') {
    //noinspection JSUnresolvedFunction
    return tc.lighten(5).toHexString();
  }else if (who==='background') {
    //noinspection JSUnresolvedFunction
    return tc.toHexString();
  }else if (who==='border') {
    //noinspection JSUnresolvedFunction
    return tc.lighten(10).toHexString();
    //
  }else if (who==='highlight') {
    //noinspection JSUnresolvedFunction
    return tc.lighten(20).toHexString();
  }else if (who==='highlight.border') {
    //noinspection JSUnresolvedFunction
    return tc.lighten(30).toHexString();
  }else if (who==='highlight.background') {
    //noinspection JSUnresolvedFunction
    return tc.lighten(20).toHexString();
    //
  }else if (who==='hover') {
    //noinspection JSUnresolvedFunction
    return tc.lighten(15).toHexString();
  }else if (who==='hover.border') {
    //noinspection JSUnresolvedFunction
    return tc.lighten(20).toHexString();
  }else if (who==='hover.background') {
    //noinspection JSUnresolvedFunction
    return tc.lighten(15).toHexString();
  }
  //noinspection JSUnresolvedFunction
  return tc.toHexString();
};

